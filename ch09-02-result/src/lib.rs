#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io;
    use std::io::Read;

    #[test]
    #[should_panic]
    fn handle_error_with_match() {
        let f = File::open("hello.txt");

        let _file = match f {
            Ok(file) => file,
            Err(error) => panic!("There was a problem opening the file: {:?}", error),
        };
    }

    #[test]
    #[should_panic]
    fn use_unwrap() {
        let _f = File::open("hello.txt").unwrap();
    }

    #[test]
    #[should_panic]
    fn use_expect() {
        let _f = File::open("hello.txt").expect("Wrong file !!!");
    }

    #[test]
    #[should_panic]
    fn propagate_error() {
        fn read_username_from_file() -> Result<String, io::Error> {
            let f = File::open("hello.txt");

            let mut f = match f {
                Ok(file) => file,
                Err(e) => return Err(e),
            };

            let mut s = String::new();

            match f.read_to_string(&mut s) {
                Ok(_) => Ok(s),
                Err(e) => Err(e),
            }
        }
        let _username = read_username_from_file().unwrap();
    }

    #[test]
    #[should_panic]
    fn propagate_error_with_question_operator() {
        fn read_username_from_file() -> Result<String, io::Error> {
            let mut f = File::open("hello.txt")?;
            let mut s = String::new();
            f.read_to_string(&mut s)?;
            Ok(s)
        }
        let _username = read_username_from_file().unwrap();
    }

    #[test]
    #[should_panic]
    fn propagate_error_in_1_line_with_question_operator() {
        fn read_username_from_file() -> Result<String, io::Error> {
            let mut s = String::new();
            File::open("hello.txt")?.read_to_string(&mut s)?;
            Ok(s)
        }
        let _username = read_username_from_file().unwrap();
    }

    #[test]
    #[should_panic]
    fn custom_error() {
        #[derive(Debug)]
        enum MyError {
            Zero,
        }
        fn get_value(val: i32) -> Result<String, MyError> {
            if val == 0 {
                return Err(MyError::Zero);
            }
            Ok(String::from("Blue"))
        }
        let value = get_value(10).unwrap();
        assert_eq!(value, "Blue");

        assert!(match get_value(0) {
            Err(MyError::Zero) => true,
            _ => false,
        });

        //This causes panic - as get_value(0) returns Err
        let _value = get_value(0).unwrap();
    }
}
