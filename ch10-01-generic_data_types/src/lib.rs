#[cfg(test)]
mod test_generics {
    #[test]
    fn non_generic_code_duplication() {
        fn largest_i32(list: &[i32]) -> i32 {
            let mut largest = list[0];

            for &item in list.iter() {
                if item > largest {
                    largest = item;
                }
            }

            largest
        }

        fn largest_char(list: &[char]) -> char {
            let mut largest = list[0];

            for &item in list.iter() {
                if item > largest {
                    largest = item;
                }
            }

            largest
        }

        let number_list = vec![34, 50, 25, 100, 65];

        let result = largest_i32(&number_list);
        assert_eq!(100, result);
        println!("The largest number is {}", result);

        let char_list = vec!['y', 'm', 'a', 'q'];

        let result = largest_char(&char_list);
        assert_eq!('y', result);
        println!("The largest char is {}", result);
    }

    /*
    #[test]
    fn generic_code_duplication() {
        fn largest<T>(list: &[T]) -> T {
            let mut largest = list[0];

            for &item in list.iter() {
                //This fails to compile:
                //binary operation `>` cannot be applied to type `T`. `T` might need a bound for `std::cmp::PartialOrd`
                if item > largest {
                    largest = item;
                }
            }

            largest
        }

        let number_list = vec![34, 50, 25, 100, 65];

        let result = largest(&number_list);
        assert_eq!(100, result);
        println!("The largest number is {}", result);

        let char_list = vec!['y', 'm', 'a', 'q'];

        let result = largest(&char_list);
        assert_eq!('y', result);
        println!("The largest char is {}", result);
    }
    */

    #[test]
    fn templated_structs() {
        struct Point<T> {
            x: T,
            y: T,
        }

        //template type was deducted - like auto in c++
        let ints = Point { x: 1, y: 100 };
        assert_eq!(ints.x, 1);
        assert_eq!(ints.y, 100);
        let floats = Point { x: 1.0, y: 100.0 };
        //Next line fails to compile with:
        //the trait `std::cmp::PartialEq<{integer}>` is not implemented for `{float}`
        //As floats.x is of float type, not int
        //assert_eq!(floats.x, 1);
        assert_eq!(floats.x, 1.0);
        assert_eq!(floats.y, 100.0);

        //Next line fails to compile with:
        //expected integral variable, found floating-point variable
        //pointing that 2nd param is incorrect
        //let mixed = Point { x: 1, y: 100.0 };
    }

    #[test]
    fn two_types_templated_structs() {
        struct Point<T, U> {
            x: T,
            y: U,
        }
        let mixed = Point { x: 1, y: 100.0 };
        assert_eq!(mixed.x, 1);
        assert_eq!(mixed.y, 100.0);
        let _ints = Point { x: 1, y: 100 };
        let _floats = Point { x: 1.0, y: 100.0 };
    }

    #[test]
    fn templated_enum() {
        enum MyResult<T> {
            Value(T),
            Empty,
        }

        let x = MyResult::Value(10);
        assert!(match x {
            MyResult::Value(10) => true,
            _ => false,
        });

        let y: MyResult<u32> = MyResult::Empty;
        assert!(match y {
            MyResult::Empty => true,
            _ => false,
        });
    }

    #[test]
    fn templated_member_functions() {
        struct Point<T> {
            x: T,
            y: T,
        }

        impl<T> Point<T> {
            fn x(&self) -> &T {
                &self.x
            }
            fn y(&self) -> &T {
                &self.y
            }
        }

        let p = Point { x: 10, y: 20 };
        assert_eq!(p.x(), &10);
        assert_eq!(p.y(), &20);

        let p = Point { x: 10.0, y: 20.0 };
        assert_eq!(p.x(), &10.0);
        assert_eq!(p.y(), &20.0);
    }

    #[test]
    fn template_specialization() {
        struct Point<T> {
            x: T,
            _y: T,
        }

        impl Point<f32> {
            fn distance(&self, other: &Point<f32>) -> f32 {
                other.x - self.x
            }
        }
        let p = Point { x: 10.0, _y: 20.0 };
        let p2 = Point { x: 20.0, _y: 20.0 };
        assert_eq!(p.distance(&p2), 10.0);

        //Fails to compile - as there is no distance for Point<integer>
        //let p3 = Point { x: 10, _y: 20 };
        //let p4 = Point { x: 20, _y: 20 };
        //assert_eq!(p3.distance(&p4), 10);
    }

    #[test]
    fn two_types_impl() {
        struct Point<T, U> {
            x: T,
            y: U,
        }
        impl<T, U> Point<T, U> {
            fn mixup<V, W>(self, other: Point<V, W>) -> Point<T, W> {
                Point {
                    x: self.x,
                    y: other.y,
                }
            }
        }
        let p1 = Point { x: 10.0, y: 20 };
        let p2 = Point { x: 10.0, y: "AAA" };
        let p3 = p1.mixup(p2);

        assert_eq!(p3.x, 10.0);
        assert_eq!(p3.y, "AAA");
    }
}
