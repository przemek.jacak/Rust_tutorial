fn mutable_string() {
    println!("mutable_string()");
    let mut s = String::from("hello");
    s.push_str(", world!"); // push_str() appends a literal to a String
    println!("{}", s); // This will print `hello, world!`
}

fn copy_simple_types() {
    println!("copy_simple_types()");
    let x = 5;
    let y = x;
    println!("{} {}", x, y); //This prints "5 5"§
}
fn move_complex_types() {
    println!("move_complex_types()");
    let x = String::from("hello");
    let y = x;
    //println!("x: {}", x); //Fails to compile - because std::string::String does not implement the `Copy` trait
    println!("y: {}", y); // prints "y: hello"
}

fn takes_ownership(some_string: String) {
    println!("{}", some_string);
}

fn makes_copy(some_integer: i32) {
    println!("{}", some_integer);
}

fn tuple_int_int(val: (i32, i32)) {
    println!("{} {}", val.0, val.1);
}
fn tuple_int_str(val: (i32, String)) {
    println!("{} {}", val.0, val.1);
}

fn ownership() {
    println!("ownership()");
    let s = String::from("ABC");
    takes_ownership(s);
    //takes_ownership(s); // fails to compile - `std::string::String` does not implement the `Copy` trait

    let x = 5;
    makes_copy(x);
    makes_copy(x); //This works fine, becasue simple type is copied

    let tup = (10, 5);
    tuple_int_int(tup);
    tuple_int_int(tup); //This also works fine, becasue tuple of simple types is copied

    let tup = (10, String::from("AAA"));
    tuple_int_str(tup);
    //tuple_int_str( tup ); //Fails to compile with 'value used here after move'
}

fn gives_ownership() -> String {
    let some_string = String::from("hello");
    some_string
}

fn takes_and_gives_back(a_string: String) -> String {
    a_string
}

fn calculate(s: String) -> (String, usize) {
    let l = s.len();
    (s, l)
}

fn return_value() {
    println!("return_value()");

    let s1 = gives_ownership();
    let s2 = takes_and_gives_back(s1);
    let (s, len) = calculate(s2);
    println!("{} {}", s, len);
}

fn calculate2(s: &String) -> usize {
    s.len()
}

fn change_str(s: &mut String) {
    s.push_str("X");
}

fn references() {
    println!("references()");
    let s = String::from("references");
    let l = calculate2(&s);
    println!("{} {}", s, l);
}

fn mutable_references() {
    println!("mutable_references()");
    let mut s = String::from("mutable_references");
    change_str(&mut s);
    println!("{}", s);
    change_str(&mut s);
    println!("{}", s);
    {
        let a = &mut s;
        println!("via a {}", a);
        //change_str( &mut s );	//Fails to compile, because s is borrowed
        change_str(a);
        //println!( "{}", s );	//Fails to compile, because s is borrowed
        println!("via a after change {}", a);
    }
    {
        let _a = &s;
        let _x = &s;
        //let x = &mut s;	//Fails to compile, becasue s is already borowed as immutable
    }
    println!("{}", s);
    //let a = &mut s; //Fails to compile - second mutable borrow occurs here
    change_str(&mut s);
    println!("{}", s);
}

//Does not compile:
//fn dangle() -> &String {
//               ^ expected lifetime parameter
//    let s = String::from("hello");
//    &s
//}

fn dangling_references() {
    println!("dangling_references()");
    //	let reference_to_nothing = dangle(); //Does not compild becasue dangle(); does not compile

    let mut s = String::from("hello");
    let mut a = &mut s;
    {
        let _x = String::from("helloX");
        //a = &mut _x; //Fails to compile - because 'borrowed value does not live long enough'
    }
    println!("{}", a);

    let _w = String::from("world");
    //    a = &mut w; //Fails to compile ... because a lives longer than w
    //             ^ borrowed value does not live long enough
}

fn main() {
    mutable_string();
    copy_simple_types();
    move_complex_types();
    ownership();
    return_value();
    references();
    mutable_references();
    dangling_references();
}
