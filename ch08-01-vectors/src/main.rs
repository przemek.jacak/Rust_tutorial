#[cfg(test)]
mod vectors_tests {
    #[test]
    fn create_with_new() {
        let v: Vec<i32> = Vec::new();
        assert_eq!(v.len(), 0);
    }

    #[test]
    fn create_with_macro() {
        let v = vec![1, 2, 3];
        assert_eq!(v.len(), 3);
        //Next line fails to compile, because v is not mutable
        //v.push(1);
    }

    #[test]
    fn push() {
        let mut v = Vec::new();
        v.push(1);
        assert_eq!(v.len(), 1);
        //Next line fails to compile, becasue v stores i32
        //v.push(1.5);
        v.push(1);
        assert_eq!(v.len(), 2);
    }

    #[test]
    fn get_item() {
        let v = vec![1, 2, 3, 4, 5];
        let third = &v[2];
        assert_eq!(*third, 3);
        let third_2 = v.get(2);
        assert!(match third_2 {
            Some(3) => true,
            _ => false,
        });
    }

    #[test]
    #[should_panic]
    fn get_item_100() {
        let v = vec![1, 2, 3, 4, 5];
        //Next line causes test to panic -> #[should_panic]
        let _item = &v[100];
    }

    #[test]
    fn get_item_100_v2() {
        let v = vec![1, 2, 3, 4, 5];
        let item = &v.get(100);
        assert!(match item {
            None => true,
            _ => false,
        });
    }

    #[test]
    fn set_item() {
        let mut v = vec![0, 1, 2, 3];
        v[0] = 100;
        assert_eq!(v[0], 100);
    }

    #[test]
    #[should_panic]
    fn set_item_100() {
        let mut v = vec![1, 2, 3, 4, 5];
        //Next line causes test to panic -> #[should_panic]
        v[100] = 100;
    }

    #[test]
    fn get_ownership() {
        let mut v = vec![1, 2, 3, 4, 5];
        {
            let _item = &v[1];
            //Next line fails to compile with : cannot borrow mutably as part of v is held at _item
            //v.push(10);
        }
        {
            let _item = v[1];
            //This works fine - as we have copy of that data
            v.push(10);
        }
        {
            let _item = v.get(100);
            //Next line fails to compile with : cannot borrow mutably as part of v is held at _item
            //v.push(10);
        }
        {
            let _item = v[0];
            //This works fine - as we have copy of that data
            v.push(10);
        }
        v.push(10);
        assert_eq!(v.len(), 8);
    }

    //run as 'cargo test -- --nocapture' to see console output
    #[test]
    fn iterate() {
        let mut v = vec![1, 3, 5, 7, 9];
        for i in &v {
            println!("{}", i);
        }
        for i in &mut v {
            *i += 10;
        }
        for i in &v {
            println!("{}", i);
            assert!(*i > 10);
        }
        assert_eq!(v.len(), 5);
    }

    //run as 'cargo test -- --nocapture' to see console output
    #[test]
    fn vector_with_enum() {
        #[derive(Debug)]
        enum SpreadsheetCell {
            Int(i32),
            Float(f64),
            Text(String),
        }

        let v = vec![
            SpreadsheetCell::Int(3),
            SpreadsheetCell::Text(String::from("blue")),
            SpreadsheetCell::Float(10.12),
        ];
        for i in &v {
            println!("{:?}", i);
            match i {
                SpreadsheetCell::Int(v) => assert_eq!(*v, 3),
                SpreadsheetCell::Text(v) => assert_eq!(*v, String::from("blue")),
                SpreadsheetCell::Float(v) => assert_eq!(*v, 10.12),
            }
        }
    }

    #[test]
    fn find_in_vector() {
        #[derive(PartialEq)]
        enum Colour {
            Red,
            Green,
            Blue,
            Black,
        }

        fn is_blue(c: &Colour) -> bool {
            /* Manual version (1)
            match c {
                Colour::Blue => true,
                _ => false,
            }
            */
            /* Manual version (2)
            if let Colour::Blue = c {
                true
            } else {
                false
            }
            */
            Colour::Blue == *c
        }

        fn blue_pos(colours: &Vec<Colour>) -> Option<usize> {
            /* Manual version (1)
            let mut res = None;
            for i in 0..colours.len() {
                if is_blue(&colours[i]) {
                    res = Some(i);
                    break;
                }
            }
            res
            */
            colours.iter().position(|ref c| is_blue(&c))
        }

        let v = vec![
            Colour::Green,
            Colour::Black,
            Colour::Blue,
            Colour::Green,
            Colour::Red,
        ];
        assert!(match blue_pos(&v) {
            Some(2) => true,
            _ => false,
        });

        let v2 = vec![Colour::Green, Colour::Black];
        assert!(match blue_pos(&v2) {
            None => true,
            _ => false,
        });
    }
}

fn main() {
    println!("ch08-01-vectors");
}
