#[cfg(test)]
mod test_triats {
    trait Export {
        fn data(&self) -> String;
    }

    trait Summary {
        fn author(&self) -> &str;

        fn summarize(&self) -> String {
            format!("reading by {}", self.author())
        }
    }

    pub struct NewsArticle {
        pub headline: String,
        pub location: String,
        pub author: String,
        pub content: String,
    }
    impl Summary for NewsArticle {
        fn summarize(&self) -> String {
            format!("{}, by {} ({})", self.headline, self.author, self.location)
        }

        fn author(&self) -> &str {
            &self.author
        }
    }

    impl Export for NewsArticle {
        fn data(&self) -> String {
            format!("{},{},{}", self.headline, self.author, self.location)
        }
    }

    fn article() -> NewsArticle {
        NewsArticle {
            headline: String::from("H"),
            location: String::from("L"),
            author: String::from("A"),
            content: String::from("C"),
        }
    }

    pub struct Tweet {
        pub username: String,
        pub content: String,
        pub reply: bool,
        pub retweet: bool,
    }
    impl Summary for Tweet {
        fn summarize(&self) -> String {
            format!("{}: {}", self.username, self.content)
        }

        fn author(&self) -> &str {
            &self.username
        }
    }
    fn tweet() -> Tweet {
        Tweet {
            username: String::from("U"),
            content: String::from("C"),
            reply: false,
            retweet: false,
        }
    }

    pub struct X {
        pub val: u32,
    }
    //This is needed so that X uses default impl for summarize, but since author
    //doesn't have default implementation it needs to be added here
    impl Summary for X {
        fn author(&self) -> &str {
            &"annonymous"
        }
    }

    #[test]
    fn test1_basic_triats() {
        let news = article();
        assert_eq!(news.summarize(), "H, by A (L)");

        let tweet = tweet();
        assert_eq!(tweet.summarize(), "U: C");

        //Next line fails with:
        // `{integer}` is a primitive type and therefore doesn't have fields
        //let i = 10;
        //assert_eq!(i.ummarize, "read...");

        let val = X { val: 10 };
        assert_eq!(val.summarize(), "reading by annonymous");
    }

    #[test]
    fn test2_triats_usage_with_impl() {
        let news = article();

        fn parse(item: impl Summary) -> String {
            //next line fails to compile, becasue item as of Summary type does not have content field
            //let v = item.content;
            format!("Breaking news! {}", item.summarize())
        }
        assert_eq!(parse(news), "Breaking news! H, by A (L)");

        //Next line fails to compile as String does not implement Summary trait
        //parse(String::from("11"));
    }

    #[test]
    //https://doc.rust-lang.org/book/ch10-02-traits.html#trait-bound-syntax
    fn test3_triats_usage_with_bound_syntax() {
        let news = article();
        //It is exacly same as parse
        fn parse2<T: Summary>(item: T) -> String {
            format!("Breaking news! {}", item.summarize())
        }
        assert_eq!(parse2(news), "Breaking news! H, by A (L)");
    }

    #[test]
    fn test4_two_params_with_impl() {
        let news = article();
        let tweet = tweet();
        fn merge(item1: &impl Summary, item2: &impl Summary) -> String {
            item1.summarize() + "| " + &item2.summarize()
        }
        assert_eq!(merge(&news, &tweet), "H, by A (L)| U: C");
    }

    #[test]
    //https://doc.rust-lang.org/book/ch10-02-traits.html#trait-bound-syntax
    fn test5_two_params_with_bond_impl() {
        let news = article();
        let tweet = tweet();
        fn merge<T: Summary, V: Summary>(item1: &T, item2: &V) -> String {
            item1.summarize() + "| " + &item2.summarize()
        }
        assert_eq!(merge(&news, &tweet), "H, by A (L)| U: C");
    }

    #[test]
    //https://doc.rust-lang.org/book/ch10-02-traits.html#specifying-multiple-trait-bounds-with-the--syntax
    fn test6_triat_bounds_with_plus() {
        let news = article();
        fn process(item: impl Summary + Export) -> String {
            item.summarize() + " -> " + &item.data()
        }
        assert_eq!(process(news), "H, by A (L) -> H,A,L")
    }

    #[test]
    //https://doc.rust-lang.org/book/ch10-02-traits.html#specifying-multiple-trait-bounds-with-the--syntax
    fn test7_triat_bounds_generic_with_plus() {
        let news = article();
        fn process<T: Summary + Export>(item: T) -> String {
            item.summarize() + " -> " + &item.data()
        }
        assert_eq!(process(news), "H, by A (L) -> H,A,L")
    }
}
