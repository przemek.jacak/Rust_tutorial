pub mod client;

pub mod network;

#[cfg(test)]
mod tests {
    use super::client;

    #[test]
    fn use_client_connect() {
        client::connect();
    }

    #[test]
    fn use_connection_status() {
        let status = client::ConnectionStatus::Connected;
        assert_eq!(status, client::ConnectionStatus::Connected);
    }
}
