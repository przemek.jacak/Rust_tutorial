#[derive(Debug, PartialEq)]
pub enum ConnectionStatus {
    Disconnected,
    Connecting,
    Connected,
    ConnectionError,
}

pub fn connect() {}
