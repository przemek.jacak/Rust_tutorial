fn first_word_by_index(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    s.len()
}

fn fw_1() {
    let mut s = String::from("hello world");
    let word = first_word_by_index(&s);
    println!("Word is: '{}'", &s[..word]);
    //Next line causes program to crash with
    //thread 'main' panicked at 'byte index 100 is out of bounds of `hello world`', libcore/str/mod.rs:2235:9
    //println!("Word is: '{}'", &s[..100] );
    s.clear();
    //This causes program to crash with
    //thread 'main' panicked at 'byte index 5 is out of bounds of ``', libcore/str/mod.rs:2235:9
    //println!("Word is: '{}'", &s[..word] );
}

fn first_word_by_slice(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}

fn fw_2() {
    let mut s = String::from("hello world");
    {
        let word = first_word_by_slice(&s);
        println!("Word is: '{}'", word);
        //Next line fails to compile - becasue 'word' has borrowed s, so 'clear' can't get it as mutable reference
        //s.clear();

        //Same if we try to modify s
        //s.push_str(" XX");
    }

    {
        let word = &s[0..5];
        println!("Word is: '{}'", word);
        //Next line fails to compile - becasue 'word' has borrowed s, so 'push_str' can't get it as mutable reference
        //s.push_str(" XX");
    }
    s.clear();
}

fn fw_3() {
    let mut a = [1, 2, 3, 4, 5];
    println!("Array: {:?}", a);
    {
        let slice = &a[1..3];
        println!("Array slice: {:?}", slice);
        //Next line fails to compile - becasue slice has borrowed a as immutable, so we can't change a
        //a[2] = 9;
        //Even next line fails to compile - despite a[4] is not within scope of slice [1,3)
        //a[4] = 10;
    }
    a[2] = 9;
    println!("Array: {:?}", a);
}

fn main() {
    fw_1();
    fw_2();
    fw_3();
}
