#[cfg(test)]
mod hash_map_tests {
    use std::collections::HashMap;

    fn create_hash_map() -> HashMap<String, i32> {
        let mut scores = HashMap::new();
        scores.insert(String::from("Blue"), 10);
        scores.insert(String::from("Yellow"), 50);
        scores
    }

    #[test]
    fn new() {
        let scores = create_hash_map();
        assert_eq!(scores.len(), 2);
    }

    #[test]
    fn zip_collect() {
        let teams = vec![String::from("Blue"), String::from("Yellow")];
        let initial_scores = vec![10, 50];

        let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();
        assert_eq!(scores.len(), 2);
    }

    #[test]
    fn get_item() {
        let scores = create_hash_map();

        let team_name = String::from("Blue");
        let score = scores.get(&team_name);
        assert!(match score {
            Some(10) => true,
            _ => false,
        });
    }

    #[test]
    fn for_loop() {
        let scores = create_hash_map();

        let mut count = 0;
        for (key, value) in &scores {
            count += 1;
            assert!(match key.as_ref() {
                "Blue" => value == &10,
                "Yellow" => value == &50,
                _ => false,
            });
        }
        assert_eq!(count, 2);
    }

    #[test]
    fn ownership() {
        let mut scores = HashMap::new();
        let red = String::from("Red");
        let red_val = String::from("#ff0000");
        scores.insert(red, red_val);

        assert_eq!(scores.len(), 1);
        //Next line fails to compile with:
        //use of moved value: `red`: value used here after move<Paste>
        //assert_eq!(red, "Red");

        //Next line fails to compile with:
        //use of moved value: `red_val`: value used here after move<Paste>
        //assert_eq!(red_val, "#ff0000");
    }

    #[test]
    fn replace() {
        let mut scores = create_hash_map();
        assert_eq!(scores["Blue"], 10);
        scores.insert(String::from("Blue"), 100);
        assert_eq!(scores["Blue"], 100);
    }

    #[test]
    fn or_insert() {
        let mut scores = create_hash_map();
        scores.entry(String::from("Pink")).or_insert(50);
        scores.entry(String::from("Blue")).or_insert(50);

        assert_eq!(scores["Blue"], 10);
        assert_eq!(scores["Pink"], 50);
    }

    #[test]
    fn word_count() {
        use std::collections::HashMap;

        let text = "hello world wonderful world";

        let mut map = HashMap::new();

        for word in text.split_whitespace() {
            let count = map.entry(word).or_insert(0);
            *count += 1;
        }

        for (key, value) in &map {
            assert!(match key.as_ref() {
                "hello" => value == &1,
                "world" => value == &2,
                "wonderful" => value == &1,
                _ => false,
            });
        }
        //println!("{:?}", map);
    }
}
