fn area_variables(width: u32, height: u32) -> u32 {
    width * height
}

fn calculate_variables() {
    let width1 = 30;
    let height1 = 50;
    println!(
        "The area of the rectangle is {} square pixels.",
        area_variables(width1, height1)
    );
}

fn area_tuple(rectangle: (u32, u32)) -> u32 {
    rectangle.0 * rectangle.1
}

fn calculate_tuple() {
    let rectangle = (30, 50);
    println!(
        "The area of the rectangle is {} square pixels.",
        area_tuple(rectangle)
    );
}

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
    fn add(&mut self, other: &Rectangle) {
        self.width += other.width;
        self.height += other.height;
    }
    //TODO: This does not compile - how to do that??
    //fn add(&mut self, other: Rectangle) {
    //    self.width += other.width;
    //    self.height += other.height;
    //}

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }

    fn new(width: u32, height: u32) -> Rectangle {
        Rectangle { width, height }
    }
}

fn area_struct(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}

fn print_rectangle(rectangle: &Rectangle) {
    //Next line fails to compile - becasue:
    //`Rectangle` cannot be formatted with the default formatter
    //println!("{}", rectangle);

    //Next line fails to compile - becasue:
    //`Rectangle` cannot be formatted using `:?`; add `#[derive(Debug)]`
    //after addint this annotation it prints as 'Rectangle { width: 30, height: 50 }'
    println!("Debug print:\n{:?}", rectangle);
    println!("Niceer debug print:\n{:#?}", rectangle);
}

fn calculate_struct() {
    let mut rectangle = Rectangle {
        width: 30,
        height: 50,
    };
    println!(
        "The area of the rectangle is {} square pixels.",
        area_struct(&rectangle)
    );

    println!(
        "The area of the rectangle is {} square pixels.",
        rectangle.area()
    );
    print_rectangle(&rectangle);

    let rect2 = Rectangle {
        width: 10,
        height: 5,
    };
    rectangle.add(&rect2);
    print_rectangle(&rectangle);
    //TODO: This does not compile - how to fix that?
    //rectangle.add(Rectangle {
    //    width: 10,
    //    height: 5,
    //});
    //print_rectangle(&rectangle);
}

fn can_hold() {
    let rect1 = Rectangle::new(30, 50);
    let rect2 = Rectangle::square(10);
    let rect3 = Rectangle {
        width: 60,
        height: 45,
    };

    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));
}

fn main() {
    calculate_variables();
    calculate_tuple();
    calculate_struct();
    can_hold();
}
