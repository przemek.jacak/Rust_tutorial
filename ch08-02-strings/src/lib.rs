#[cfg(test)]
mod string_tests {
    #[test]
    fn push() {
        let mut s = String::from("foo");
        let mut size = s.len();
        s.push_str("bar");
        assert_eq!(s, "foobar");
        assert!(s.len() > size);
        size = s.len();

        let s2 = "bar";
        s.push_str(s2);
        assert_eq!(s, "foobarbar");
        assert!(s.len() > size);
        size = s.len();

        s.push('!');
        assert_eq!(s, "foobarbar!");
        assert!(s.len() > size);
    }

    #[test]
    fn plus() {
        let s1 = String::from("Hello, ");
        let s2 = String::from("world!");
        let s3 = s1 + &s2;
        assert_eq!(s3, "Hello, world!");
        //Next line fails to ompile - as s1 is used after move!
        //assert_eq!(s1.len(), 7);
        assert_eq!(s2.len(), 6);
        assert_eq!(s3.len(), 13);
    }

    #[test]
    fn tic_tac_toe_with_plus() {
        let s1 = String::from("tic");
        let s2 = String::from("tac");
        let s3 = String::from("toe");

        //Next line fails to compile as there is no add operator for str
        //let s = "" + &s1 + "-" + &s2 + "-" + &s3;

        //This way s1 is still ok:
        let s = String::new() + &s1 + "-" + &s2 + "-" + &s3;
        assert_eq!(s, "tic-tac-toe");
        assert_eq!(s1.len(), 3);
        assert_eq!(s2.len(), 3);
        assert_eq!(s3.len(), 3);
    }

    #[test]
    fn tic_tac_toe_with_format() {
        let s1 = String::from("tic");
        let s2 = String::from("tac");
        let s3 = String::from("toe");
        let s = format!("{}-{}-{}", s1, s2, s3);
        assert_eq!(s, "tic-tac-toe");
        assert_eq!(s1.len(), 3);
        assert_eq!(s2.len(), 3);
        assert_eq!(s3.len(), 3);
    }

    #[test]
    fn indexing_slicing() {
        let s1 = String::from("hello");
        //Next line fails to compile with: the type `std::string::String` cannot be indexed by `{integer}`
        //let h = s1[0];
        //But you can have it as slice
        let h = &s1[0..1];
        assert_eq!(h, "h");
    }

    #[test]
    #[should_panic]
    fn slice_of_unicode() {
        let s1 = String::from("Łódź");
        //Next line panics with "panicked at 'byte index 1 is not a char boundary"
        let _a = &s1[0..1];
    }

    #[test]
    fn iterate_via_chars() {
        let s1 = String::from("Łódź");
        let mut count = 0;
        for _c in s1.chars() {
            count = count + 1;
        }
        assert_eq!(count, 4);
    }

    #[test]
    fn iterate_as_bytes() {
        let s1 = String::from("Łódź");
        let mut count = 0;
        for _c in s1.bytes() {
            count = count + 1;
        }
        //Not all charsare in UTF-8 so it's not 2 * size
        assert_eq!(count, 7); //2:'Ł', 2:'ó', 1:'d', 2:'ź'
    }
}
