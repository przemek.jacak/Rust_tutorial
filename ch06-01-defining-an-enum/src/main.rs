#[cfg(test)]
mod enum_tests {

    enum Colour {
        Red,
        Green,
        Blue,
    }

    fn is_blue(c: Colour) -> bool {
        match c {
            Colour::Blue => true,
            _ => false,
        }
    }

    #[test]
    fn enum_comapre() {
        assert_eq!(is_blue(Colour::Red), false);
        assert_eq!(is_blue(Colour::Blue), true);
    }
}

#[derive(Debug)]
enum IpAddrKindV1 {
    V4,
    V6,
}

fn print_ip_addr_kind(addr_kind: &IpAddrKindV1) {
    println!("It's {:?}", addr_kind);
    //    if (addr_kind == IpAddrKindV1::V4) {
    //        println!("It's IpV4 addr");
    //    } else if (addr_kind == IpAddrKindV1::V6) {
    //        println!("It's IpVr6 addr");
    //    }
}

fn simple_enums() {
    println!("-------------------");
    println!("simple_enums()");
    let four = IpAddrKindV1::V4;
    print_ip_addr_kind(&four);
    let six = IpAddrKindV1::V6;
    print_ip_addr_kind(&six);
    //Next line fails to compile
    //six++;
    //    ^ expected expression

    //Next line fails to compile:
    //binary assignment operation `+=` cannot be applied to type `IpAddrKindV1`
    //six += 1;

    //Next line fails to compile:
    //binary operation `+` cannot be applied to type `IpAddrKindV1`
    //six = six + 1;

    print_ip_addr_kind(&six);

    print_ip_addr_kind(&IpAddrKindV1::V4);
    print_ip_addr_kind(&IpAddrKindV1::V6);
}

#[derive(Debug)]
enum IpAddr {
    V4(String),
    V6(String),
}

fn enum_with_data() {
    println!("-------------------");
    println!("enum_with_data()");
    let home = IpAddr::V4(String::from("127.0.0.1"));
    println!("It's {:?}", home);
    let loopback = IpAddr::V6(String::from("::1"));
    println!("It's {:?}", loopback);
}

#[derive(Debug)]
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

impl Message {
    fn call(&self) {
        println!("called with {:?}", self);
    }
}

fn enum_with_impl() {
    println!("-------------------");
    println!("enum_with_impl()");
    let m = Message::Write(String::from("hello"));
    m.call();
}

fn options() {
    println!("-------------------");
    println!("options()");

    let some_number = Some(5);
    println!("{:?}", some_number);

    let some_string = Some("a string");
    println!("{:?}", some_string);

    let absent_number: Option<i32> = None;
    println!("{:?}", absent_number);

    //Next line fails to compile - can't assign i32 from std::option::Option<{integer}>
    //let x: i32 = some_number;

    //Line with sum fails - becasue you can't add i32 to std::option::Option<{integer}>
    //as Option might not have value
    //let y: i32 = 5;
    //let sum = y + some_number;
}

fn main() {
    simple_enums();
    enum_with_data();
    enum_with_impl();
    options();
}
