fn printer1(data: Option<i32>) {
    match data {
        Some(3) => println!("three"),
        Some(v) => println!("i32({})", v),
        _ => (),
    }
}

fn printer2(data: Option<i32>) {
    if let Some(3) = data {
        println!("three");
    } else if let Some(v) = data {
        println!("i32({})", v);
    }
}

fn main() {
    let some_8_value = Some(8);
    printer1(some_8_value);
    printer1(None);
    printer1(Some(3));

    println!("---------------");
    printer2(some_8_value);
    printer2(None);
    printer2(Some(3));
}
