struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user(email: String, username: String) -> User {
    User {
        email: email,
        username: username,
        active: true,
        sign_in_count: 1,
    }
}

fn build_user2(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn construct() {
    {
        let user = User {
            email: String::from("someone@example.com"),
            username: String::from("someusername123"),
            active: true,
            sign_in_count: 1,
        };

        println!("Hello, {}!", user.username);
        //Next line fials to compile- becasue user is immutable
        //user.username = String::from("AAA");
    }
    {
        let mut user = User {
            email: String::from("someone@example.com"),
            username: String::from("someusername123"),
            active: true,
            sign_in_count: 1,
        };

        user.username = String::from("AAA");
        println!("Hello, {}!", user.username);
    }
    {
        let mut user = build_user(
            String::from("someone@example.com"),
            String::from("username123"),
        );
        println!("Hello, {}!", user.username);
        user.username = String::from("AAA");
        println!("Hello, {}!", user.username);
    }
    {
        let mut user = build_user2(
            String::from("someone@example.com"),
            String::from("username123"),
        );
        println!("Hello, {}!", user.username);
        user.username = String::from("AAA");
        println!("Hello, {}!", user.username);
    }
}

fn copy_constructor() {
    {
        let mut user = build_user2(
            String::from("someone@example.com"),
            String::from("username123"),
        );
        let user2 = User {
            email: String::from("another@example.com"),
            username: String::from("anotherusername567"),
            active: user.active,
            sign_in_count: user.sign_in_count,
        };
        println!("Hello, {}!", user2.username);
        user.sign_in_count += 1;
        let user3 = User {
            email: String::from("another@example.com"),
            username: String::from("anotherusername7"),
            ..user
        };
        println!(
            "Hello, {}! It's your {} login",
            user3.username, user3.sign_in_count
        );
    }
}

//Tuple Structs
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

fn print_color(color: &Color) {
    println!("Color: {},{},{}", color.0, color.1, color.2);
}
fn print_point(point: &Point) {
    //TODO: destructure does not work, despite:
    //tuple struct instances behave like tuples: you can destructure them into their individual pieces, you can use a . followed by the index to access an individual value, and so on.
    //let (x, y, z) = point;
    //println!( "Point: {},{},{}", x, y, z);
    println!("Color: {},{},{}", point.0, point.1, point.2);
}

fn print_tuple(val: &(i32, i32, i32)) {
    println!("Tuple: {},{},{}", val.0, val.1, val.2);
}

fn tuple_structs() {
    let black = Color(0, 0, 0);
    print_color(&black);

    let origin = Point(0, 0, 0);
    print_point(&origin);

    //Next line fails to compile, becasue expected struct `Point`, found struct `Color`
    //print_point( &black );
    //Next line does not compile, becasue expected tuple, found struct `Point`
    //print_tuple( &origin );
}

fn main() {
    construct();
    copy_constructor();
    tuple_structs();
}
