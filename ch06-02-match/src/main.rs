enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter,
}

fn value_in_cents(coin: Coin) -> u32 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter => 25,
    }
}

fn coint_machine_1() {
    println!("------------------------");
    println!("Hello, penny ({})", value_in_cents(Coin::Penny));
    println!("Hello, nickel ({})", value_in_cents(Coin::Nickel));
    println!("Hello, dime ({})", value_in_cents(Coin::Dime));
    println!("Hello, quarter ({})", value_in_cents(Coin::Quarter));
}

#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
}

enum Coin2 {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents2(coin: Coin2) -> u32 {
    match coin {
        Coin2::Penny => 1,
        Coin2::Nickel => 5,
        Coin2::Dime => 10,
        Coin2::Quarter(state) => {
            println!("Quarter from {:?}", state);
            25
        }
    }
}

fn coint_machine_2() {
    println!("------------------------");
    println!("Hello, penny ({})", value_in_cents2(Coin2::Penny));
    println!("Hello, nickel ({})", value_in_cents2(Coin2::Nickel));
    println!("Hello, dime ({})", value_in_cents2(Coin2::Dime));
    println!(
        "Hello, quarter ({})",
        value_in_cents2(Coin2::Quarter(UsState::Alabama))
    );
    println!(
        "Hello, quarter ({})",
        value_in_cents2(Coin2::Quarter(UsState::Alaska))
    );
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        Some(i) => Some(i + 1),
        None => None,
    }
}

fn match_option() {
    println!("------------------------");
    let five = Some(5);
    println!(" five = {:?}", five);
    let six = plus_one(five);
    println!(" six = {:?}", six);
    let none = plus_one(None);
    println!(" none = {:?}", none);
}

fn main() {
    coint_machine_1();
    coint_machine_2();
    match_option();
}
